// SPDX-FileCopyrightText: 2024 L. E. Segovia <amy@amyspark.me>
// SPDX-License-Identifier: BSD-3-Clause

#include <llvm-c/lto.h>
#include <string_view>
#include <vector>

#include <substrate/command_line/arguments>
#include <substrate/console>
#include <substrate/fd>
#include <substrate/pointer_utils>

#include "config.h"

using substrate::console;
using namespace substrate::commandLine;
using namespace std::string_view_literals;

constexpr static auto programOptions =
    options(option_t{optionFlagPair_t{"-h"sv, "--help"sv}, "Display this help message and exit"sv},
            option_t{"--version"sv, "Display the program version information and exit"sv},
            option_t{"-o"sv, "Destination of the merged object file"sv}.takesParameter(optionValueType_t::path),
            option_t{"-keep-symbol"sv, "Preserve this symbol"sv}.takesParameter(optionValueType_t::string).repeatable(),
            option_t{optionValue_t{"objectFile"sv}, "Combine these object files"sv}.takesParameter(optionValueType_t::path).repeatable());

static void displayHelp() noexcept
{
    console.writeln("dragonfire - Utility to perform Single-Object Prelinking on Windows"sv);
    console.writeln();
    console.writeln("Usage:"sv);
    console.writeln("\tdragonfire [options] objectFile..."sv);
    console.writeln();
    programOptions.displayHelp();
    console.writeln(""sv);
    console.writeln("This utility is licensed under BSD-3-Clause"sv);
    console.writeln("Please report bugs to https://gitlab.freedesktop.org/amyspark/dragonfire/-/issues"sv);
}

static void lto_release(lto_code_gen_t *l)
{
    lto_codegen_dispose(*l);
    *l = nullptr;
};

int main(int argc, const char *const *const argv)
{
    console = {stdout, stderr};

    // Try to parser the command line arguments
    const auto result = parseArguments(static_cast<size_t>(argc), argv, programOptions);

    if (!result) {
        console.error("Failed to parse command line arguments"sv);
        return 1;
    }

    // Obtain the list of arguments
    const auto args{*result};

    // Handle the version and help options first
    const auto *const version = args["version"sv];
    const auto *const help = args["help"sv];
    if (version && help) {
        console.error("Can only specify one of --help and --version, not both."sv);
        return 1;
    }

    if (version) {
        static constexpr std::string_view compiler_version{COMPILER_VERSION};
        static constexpr std::string_view program_version{VERSION};
        console.writeln("dragonfire "sv, program_version);
        console.writeln("Compiled with "sv, compiler_version);
        console.writeln("Using "sv, lto_get_version());
        return 0;
    }

    // Display the help if requested or there were no command line options given
    if (help || args.count() == 0U) {
        displayHelp();
        return 0;
    }

    const auto input = args.findAll("objectFile"sv);
    std::vector<std::string> objects;
    objects.reserve(input.size());

    for (const auto &i : input) {
        auto path = std::any_cast<std::filesystem::path>(*i).u8string();
        const auto can = lto_module_is_object_file(path.c_str());
        console.debug(path, ": is readable: "sv, can ? "yes"sv : "no"sv);
        if (can) {
            objects.emplace_back(path);
        }
    }

    if (!objects.empty()) {
        auto lto = lto_codegen_create();

        if (!lto) {
            console.error(lto_get_error_message());
            return 1;
        }

        substrate::nicePtr_t<decltype(&lto_release), &lto_release> guard{&lto};

        for (const auto &i : objects) {
            auto module = lto_module_create(i.c_str());
            if (!module) {
                console.error(lto_get_error_message());
                return 1;
            }
            const auto error = lto_codegen_add_module(lto, module);
            if (error) {
                console.error(lto_get_error_message());
                return 1;
            }
        }

        const auto symbols = args.findAll("keep-symbol"sv);

        for (const auto &i : input) {
            auto symbol = std::any_cast<std::string>(*i);
            lto_codegen_add_must_preserve_symbol(lto, symbol.c_str());
            console.debug("Preserving symbol: "sv, symbol);
        }

        const auto file =
            substrate::fd_t(std::any_cast<std::filesystem::path>(std::get<flag_t>(*args["o"sv]).value()), O_WRONLY | O_CREAT | O_NOCTTY, substrate::normalMode);

        if (!file) {
            console.error("Failed to open destination object file");
            return 1;
        }

        size_t sz = 0;
        const auto buffer = lto_codegen_compile(lto, &sz);
        if (sz == 0 || buffer == nullptr) {
            console.error(lto_get_error_message());
            return 1;
        }

        if (!file.write(buffer, sz)) {
            console.error("Cannot write output file: out of space?"sv);
            return 1;
        }
    }

    return 0;
}
